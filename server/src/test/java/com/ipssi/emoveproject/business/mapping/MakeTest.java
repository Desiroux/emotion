package com.ipssi.emoveproject.business.mapping;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MakeTest {

    private final String code = "C569";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void should_create_make() {
        //Given When
        Make make = new Make(code);

        // Then
        assertThat(make.getCode()).isEqualTo(code);
    }

    @Test
    public void should_throw_exception_when_code_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Code cannot be blank");

        // When
        new Make("");
    }

    @Test
    public void should_throw_exception_when_code_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Code cannot be blank");

        // When
        new Make(null);
    }

}