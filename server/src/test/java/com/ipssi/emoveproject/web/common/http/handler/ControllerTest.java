package com.ipssi.emoveproject.web.common.http.handler;

import com.google.gson.Gson;
import lombok.Getter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("file:server/src/main/webapp")
@TestPropertySource("classpath:log4j.properties")
public abstract class ControllerTest {

    protected static final Gson gson;

    static {
        gson = new Gson();
    }

    @Getter
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(context).build();
    }

    protected static MultiValueMap<String, String> toQueryParams(Serializable serializable) {
        final MultiValueMap<String, String> map;

        map = new LinkedMultiValueMap<>();

        stream(serializable.getClass().getDeclaredFields()).forEach(field -> {
            final Object value;

            field.setAccessible(true);

            try {
                value = field.get(serializable);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            if(value != null)
                map.put(field.getName(), asList(value.toString()));
        });

        return map;
    }
}
