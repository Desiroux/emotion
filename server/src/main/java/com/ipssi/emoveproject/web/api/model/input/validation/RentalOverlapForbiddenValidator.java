package com.ipssi.emoveproject.web.api.model.input.validation;

import com.albema.common.orm.entity.Void;
import com.albema.spring.validation.BusinessDataValidator;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;
import java.lang.reflect.Field;

class RentalOverlapForbiddenValidator extends BusinessDataValidator<RentalOverlapForbidden> {

    @Override
    public boolean isValid(Serializable serializable, ConstraintValidatorContext constraintValidatorContext) {
        final Class<? extends Serializable> serializableClass;

        final Field pickupDateField;
        final Field vehicleIdField;
        final Field durationField;

        final String stm;

        final Query<Boolean> query;

        serializableClass = serializable.getClass();

        try {
            pickupDateField = serializableClass.getDeclaredField(getConstraintAnnotation().pickupDateAttribute());
            vehicleIdField = serializableClass.getDeclaredField(getConstraintAnnotation().vehicleIdAttribute());
            durationField = serializableClass.getDeclaredField(getConstraintAnnotation().durationAttribute());

            pickupDateField.setAccessible(true);
            vehicleIdField.setAccessible(true);
            durationField.setAccessible(true);

            stm = "SELECT doesRentalOverlap(CAST(:pickupDate AS date), CAST(:duration AS short), CAST(:vehicleId AS long)) " +
                  "FROM " + Void.class.getName();

            query = getCurrentSession().createQuery(stm, Boolean.class);

            query.setParameter("pickupDate", pickupDateField.get(serializable));

            query.setParameter("duration", durationField.get(serializable));

            query.setParameter("vehicleId", vehicleIdField.get(serializable));

        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return !query.uniqueResult();
    }

    RentalOverlapForbiddenValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
