package com.ipssi.emoveproject.web.common.business.repository;

import com.albema.spring.repository.DAO;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.business.repository.ModelDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
class ModelDAOImpl extends DAO implements ModelDAO {

    @Override
    public List<Model> find() {
        return find0(Model.class);
    }

    @Override
    public List<Model> find(BigInteger... ids) {
        return find0(Model.class, ids);
    }

    @Override
    public Model find(BigInteger id) {
        return find0(Model.class, id);
    }

    @Override
    public List<Model> findByMakeId(BigInteger makeId) {
        final String stm;

        stm = "FROM " + Model.class.getName() + " " +
              "WHERE make.id = :makeId";

        return getCurrentSession().createQuery(stm, Model.class)
                                  .setParameter("makeId", makeId)
                                  .list();
    }

    @Override
    public List<Model> findByNameLike(String name) {
        final String stm;

        stm = "FROM " + Model.class.getName() + " " +
              "WHERE LOWER(name) LIKE LOWER(:nameLike)";

        return getCurrentSession().createQuery(stm, Model.class)
                                  .setParameter("nameLike", name + "%")
                                  .list();
    }

    @Override
    public List<Model> findByMakeIdAndNameLike(BigInteger makeId, String name) {
        final String stm;

        stm = "FROM " + Model.class.getName() + " " +
              "WHERE make.id=:makeId AND LOWER(name) LIKE LOWER(:nameLike)";

        return getCurrentSession().createQuery(stm, Model.class)
                                  .setParameter("makeId", makeId)
                                  .setParameter("nameLike", name + "%")
                                  .list();
    }

    @Override
    public List<Model> findByVehicleType(VehicleType vehicleType) {
        final String stm;

        stm = "FROM " + Model.class.getName() + " " +
              "WHERE vehicleType=:vehicleType " +
              "ORDER BY name";

        return getCurrentSession().createQuery(stm, Model.class)
                                  .setParameter("vehicleType", vehicleType)
                                  .list();
    }

    @Override
    public int remove(BigInteger... ids) {
        return remove0(Model.class, ids);
    }

    @Override
    public boolean remove(BigInteger id) {
        return remove0(Model.class, id) != 0;
    }

    @Override
    public boolean remove(Model model) {
        return remove0(model);
    }

    @Override
    public int remove(Model... models) {
        return remove0(models);
    }

    @Override
    public void save(Model entity) {
        save0(entity);
    }

    @Override
    public void save(Model... entities) {
        save0(entities);
    }

    ModelDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
