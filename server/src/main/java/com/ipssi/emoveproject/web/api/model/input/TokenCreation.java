package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.ExistingEntity;
import com.ipssi.emoveproject.business.mapping.User;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TokenCreation implements Serializable {

    @ExistingEntity(
            value = User.class,
            identifierProperty = "emailAddress"
    )
    private String emailAddress;

    @NotBlank
    private String password;
}
