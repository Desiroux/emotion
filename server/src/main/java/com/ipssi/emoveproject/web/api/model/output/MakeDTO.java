package com.ipssi.emoveproject.web.api.model.output;

import com.ipssi.emoveproject.business.mapping.Make;

import java.math.BigInteger;

@SuppressWarnings("FieldCanBeLocal")
public class MakeDTO extends DTO<Make> {

    private final String code;

    private final String name;

    private final BigInteger[] modelIds;

    public MakeDTO(Make make) {
        super(make);

        code = make.getCode();
        name = make.getName();

        modelIds = getIds(make.getModels());
    }
}
