package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.logic.UserService;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@RequiredArgsConstructor
public class CustomerDetailsUpdate extends CustomerUpdate implements UserService.CustomerDetailsUpdate {

    @NotBlank
    private String address;

    @NotBlank
    private String city;

    @NotEmpty
    @Pattern(regexp = "^\\d{5}$")
    private String zipCode;
}
