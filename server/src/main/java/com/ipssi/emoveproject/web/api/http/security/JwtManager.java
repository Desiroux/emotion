package com.ipssi.emoveproject.web.api.http.security;

import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.User;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;

import static io.jsonwebtoken.Jwts.builder;
import static io.jsonwebtoken.Jwts.parser;
import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static java.time.Instant.*;
import static org.slf4j.LoggerFactory.getLogger;

@Component
public class JwtManager {

    private final String secret;

    private final long tokenExpirationTime;

    private final UserService userService;

    User parseToken(String token) {
        final Claims claims;
        final User user;

        claims = parser().setSigningKey(secret)
                         .parseClaimsJws(token)
                         .getBody();

        user = userService.getOne(new BigInteger(claims.getSubject()));

        return user;
    }

    public Pair<String, Instant> generateToken(Authentication authentication) {

        final org.springframework.security.core.userdetails.User userPrincipal;
        final Instant now;
        final Instant expirationTime;
        final String  token;

        userPrincipal = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();

        now = now();
        expirationTime = now.plusSeconds(tokenExpirationTime);

        token = builder().setSubject(userPrincipal.getUsername())
                         .setIssuedAt(new Date(now.toEpochMilli()))
                         .setExpiration(new Date(expirationTime.toEpochMilli()))
                         .signWith(HS512, secret)
                         .compact();

        return new ImmutablePair<>(token, expirationTime);
    }

    boolean validateToken(String authToken) {
        try {
            parser().setSigningKey(secret)
                    .parseClaimsJws(authToken);

            return true;
        } catch (JwtException | IllegalArgumentException ex) {
            return false;
        }
    }

    JwtManager(
            @Value("${jwt.secret}")         String      secret,
            @Value("${jwt.expirationTime}") long        tokenExpirationTime,
                                            UserService userService
    ) {
        this.secret = secret;
        this.tokenExpirationTime = tokenExpirationTime;
        this.userService = userService;
    }
}
