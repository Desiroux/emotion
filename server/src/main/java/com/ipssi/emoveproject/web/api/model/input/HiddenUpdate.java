package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.logic.VehicleService;
import lombok.*;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class HiddenUpdate extends VehicleUpdate implements VehicleService.HiddenUpdate {

    private boolean hidden;
}
