package com.ipssi.emoveproject.web.api.http.security;

import com.ipssi.emoveproject.business.mapping.Admin;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.User;
import lombok.Getter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.springframework.security.core.authority.AuthorityUtils.NO_AUTHORITIES;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {

    @Getter
    private String token;

    @Getter
    private User user;

    JwtAuthenticationToken(String token) {
            super(null, null, NO_AUTHORITIES);
            this.token = token;
    }

    public JwtAuthenticationToken(User user) {
        super(user.getId(), user.getEmailAddress(),
                createAuthorityList((
                                user instanceof Admin ?
                                        Admin.class :
                                        Customer.class
                ).getSimpleName().toUpperCase())
        );
        setAuthenticated(true);

        this.user = user;
    }
}
