package com.ipssi.emoveproject.web.files.model.input.validation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.ArrayUtils.contains;

class ValidFileValidator implements ConstraintValidator<ValidFile, MultipartFile> {

    private final long     maxSize;

    private final String[] allowedContentTypes;

    @Override
    public boolean isValid(
            MultipartFile multipartFile,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        return multipartFile.getSize() <= maxSize &&
                contains(allowedContentTypes, multipartFile.getContentType());
    }

    ValidFileValidator(
            @Value("${maxSize}")             long     maxSize,
            @Value("${allowedContentTypes}") String[] allowedContentTypes) {
        this.maxSize             = maxSize;
        this.allowedContentTypes = allowedContentTypes;
    }
}
