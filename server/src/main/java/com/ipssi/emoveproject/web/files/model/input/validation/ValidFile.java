package com.ipssi.emoveproject.web.files.model.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ValidFileValidator.class)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidFile {
    String message() default "ValidFile";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
