package com.ipssi.emoveproject.web.common.business.repository;

import com.albema.spring.repository.DAO;
import com.ipssi.emoveproject.business.mapping.User;
import com.ipssi.emoveproject.business.repository.UserDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
class UserDAOImpl extends DAO implements UserDAO {

    @Override
    public void save(User entity) {
        save0(entity);
    }

    @Override
    public void save(User... entities) {
        save0(entities);
    }

    @Override
    public List<User> find() {
        return find0(User.class);
    }

    @Override
    public List<User> find(BigInteger... ids) {
        return find0(User.class, ids);
    }

    @Override
    public boolean remove(User entity) {
        return remove0(entity);
    }

    @Override
    public User find(BigInteger id) {
        return find0(User.class, id);
    }

    @Override
    public int remove(User... entities) {
        return remove0(entities);
    }

    @Override
    public boolean remove(BigInteger id) {
        return remove0(User.class, id) == 0;
    }

    @Override
    public int remove(BigInteger... ids) {
        return remove0(User.class, ids);
    }

    @Override
    public User findByEmailAddress(String emailAddress) {
        final String stm;

        stm = "FROM " + User.class.getName() + " "
            + "WHERE emailAddress=:emailAddress";
        return getCurrentSession().createQuery(stm, User.class)
                                  .setParameter("emailAddress", emailAddress)
                                  .uniqueResult();
    }

    @Override
    public User findByEmailAddressAndHash(String emailAddress, String hash) {
        final String stm;

        stm = "FROM " + User.class.getName() + " "
            + "WHERE emailAddress=:emailAddress AND hash=:hash";

        return getCurrentSession().createQuery(stm, User.class)
                                  .setParameter("emailAddress", emailAddress)
                                  .setParameter("hash", hash)
                                  .uniqueResult();
    }

    @Override
    public <U extends User> List<U> find(Class<U> userClass) {
        return find0(userClass);
    }

    @Override
    public <U extends User> U find(Class<U> userClass, BigInteger id) {
        return find0(userClass, id);
    }

    @Override
    public <U extends User> List<U> find(Class<U> userClass, BigInteger[] ids) {
        return find0(userClass, ids);
    }

    UserDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
