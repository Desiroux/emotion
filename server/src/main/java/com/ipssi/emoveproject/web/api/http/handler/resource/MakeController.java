package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.web.api.model.input.MakeCreation;
import com.ipssi.emoveproject.web.api.model.output.MakeDTO;
import com.ipssi.emoveproject.web.api.model.output.ModelDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.math.BigInteger;
import java.util.List;

import static com.albema.common.util.ObjectUtils.ifNotNull;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/makes")
class MakeController {

    private final VehicleService vehicleService;
    
    @GetMapping
    public List<MakeDTO> onGet() {
        return fromCollection(vehicleService.getAllMakes(), MakeDTO::new);
    }

    @GetMapping(params = "ids")
    public List<MakeDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(vehicleService.getMakes(ids), MakeDTO::new);
    }

    @GetMapping("/{id}")
    public MakeDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(vehicleService.getOneMake(id), MakeDTO::new);
    }

    @GetMapping(params = "name")
    public List<MakeDTO> onGet(@RequestParam String name) {
        return fromCollection(
                vehicleService.getMakes(name),
                MakeDTO::new
        );
    }

    @GetMapping(params = "vehicleType")
    public List<MakeDTO> onGet(@RequestParam VehicleType vehicleType) {
        return fromCollection(
                vehicleService.getMakes(vehicleType),
                MakeDTO::new
        );
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(CREATED)
    @PostMapping
    public MakeDTO onPost(@Valid @RequestBody MakeCreation creation) {
        return new MakeDTO(vehicleService.createMake(creation));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(
            @PathVariable                 BigInteger id,
            @Valid @NotBlank @RequestBody String     newName
    ) {
        vehicleService.updateMake(id, newName);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void onDelete(@PathVariable BigInteger id) {
        vehicleService.deleteMakes(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(params = "ids")
    public void onDelete(@RequestParam BigInteger[] ids) {
        vehicleService.deleteMakes(ids);
    }

    @GetMapping("/{makeId}/models")
    public List<ModelDTO> onModelGet(@PathVariable BigInteger makeId) {
        return fromCollection(vehicleService.getModels(makeId), ModelDTO::new);
    }
}
