package com.ipssi.emoveproject.web.api.model.input;

import com.google.gson.annotations.JsonAdapter;
import com.ipssi.emoveproject.business.logic.UserService;

import java.io.Serializable;

@JsonAdapter(GsonPolymorphicAdapter.class)
public abstract class CustomerUpdate implements UserService.CustomerUpdate, Serializable {}
