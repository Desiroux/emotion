package com.ipssi.emoveproject.web.common.business.logic;

import com.albema.common.orm.repository.Finder;
import com.ipssi.emoveproject.business.logic.RentalService;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.Rental;
import com.ipssi.emoveproject.business.mapping.User;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import com.ipssi.emoveproject.business.repository.RentalDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import static lombok.AccessLevel.PACKAGE;

@Service
@AllArgsConstructor(access = PACKAGE)
class RentalServiceImpl implements RentalService {

    private final RentalDAO rentalDAO;

    private final Finder<Vehicle> vehicleFinder;

    private final Finder<User> userFinder;

    @Override
    @Transactional(readOnly = true)
    public List<Rental> get(BigInteger... ids) {
        return rentalDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rental> getAll() {
        return rentalDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public Rental getOne(BigInteger id) {
        return rentalDAO.find(id);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Rental> getByCustomerId(BigInteger customerId) {
        return rentalDAO.findByCustomerId(customerId);
    }

    @Override
    @Transactional(readOnly = true)
    public Rental getOneByCustomerId(BigInteger rentalId, BigInteger customerId) {
        return rentalDAO.findByCustomerId(rentalId, customerId);
    }

    @Override
    @Transactional
    public Rental create(Creation creation) {
        final Vehicle vehicle;
        final Customer customer;
        final Rental rental;

        vehicle = vehicleFinder.find(creation.getVehicleId());
        customer = (Customer) userFinder.find(creation.getCustomerId());

        rental = new Rental(creation.getPickupDate(), creation.getDuration(), vehicle, customer);

        rental.setUsingLoyaltyPoints(creation.isUsingLoyaltyPoints());

        rentalDAO.save(rental);

        return rental;
    }

    @Override
    @Transactional
    public void update(BigInteger id, Update update) {
        final Rental rental;

        rental = rentalDAO.find(id);

        if(rental == null)
            return;

        if(update instanceof OverdueDurationUpdate)
            rental.setNumberOfDaysLate(((OverdueDurationUpdate) update).getOverdueDuration());

        rentalDAO.save(rental);
    }

    @Override
    @Transactional
    public void delete(BigInteger... ids) {
        rentalDAO.remove(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rental> get(
            LocalDate maxExclusiveDropOffDate,
            Integer   overdueDuration
    ) {
        return rentalDAO.find(maxExclusiveDropOffDate, overdueDuration);
    }
}
