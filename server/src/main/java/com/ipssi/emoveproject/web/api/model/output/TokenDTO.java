package com.ipssi.emoveproject.web.api.model.output;

import com.google.gson.annotations.SerializedName;
import com.ipssi.emoveproject.business.mapping.Admin;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.User;
import lombok.NonNull;

import java.io.Serializable;
import java.time.Instant;

@SuppressWarnings(
        "FieldCanBeLocal"
)
public class TokenDTO implements Serializable {

    @SerializedName("user")
    private UserDTO userDTO;

    private String  value;

    private Instant expirationTime;

    public TokenDTO(@NonNull User user, String value, Instant expirationTime) {
        this.userDTO = user instanceof Customer ?
                new CustomerDTO((Customer) user) :
                new AdminDTO((Admin) user);
        this.value = value;
        this.expirationTime = expirationTime;
    }
}
