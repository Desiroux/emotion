package com.ipssi.emoveproject.business.logic;

import com.ipssi.emoveproject.business.mapping.Admin;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.User;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;

public interface UserService {

    interface UserCreation {

        String getEmailAddress();

        String getPassword();
    }

    interface AdminCreation extends UserCreation {
    }

    interface CustomerCreation extends UserCreation {

        String getFirstName();

        String getLastName();

        LocalDate getBirthDate();

        String getAddress();

        String getZipCode();

        String getCity();
    }

    interface CustomerDetailsUpdate extends CustomerUpdate {

        String getAddress();

        String getZipCode();

        String getCity();
    }

    interface TokenCreation {

        String getEmailAddress();

        String getPassword();
    }

    User getOne(BigInteger id);

    User getOne(String emailAddress);

    User getOne(String emailAddress, String password) throws NoSuchAlgorithmException;

    void delete(BigInteger... ids);

    void update(BigInteger id, String newPassword) throws NoSuchAlgorithmException;

    List<Customer> getCustomers(BigInteger... ids);

    List<Customer> getAllCustomers();

    Customer getOneCustomer(BigInteger id);

    Customer createCustomer(CustomerCreation creation) throws NoSuchAlgorithmException;

    void updateCustomer(BigInteger id, CustomerUpdate update);

    void updateCustomer(BigInteger id, boolean mailValidated);

    interface CustomerUpdate {
    }

    interface GDPRUpdate extends CustomerUpdate {
    }

    List<Admin> getAdmins(BigInteger... ids);

    List<Admin> getAllAdmins();

    Admin getOneAdmin(BigInteger id);

    Admin createAdmin(AdminCreation creation) throws NoSuchAlgorithmException;
}
