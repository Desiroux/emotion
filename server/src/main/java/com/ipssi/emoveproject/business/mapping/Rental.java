package com.ipssi.emoveproject.business.mapping;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

import static java.util.Objects.requireNonNull;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

@Getter
@Entity
@Table(
        name = "rentals",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {
                        "pickup_date",
                        "vehicle_id"
                }
        )
)
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode(
        of = {"pickupDate", "vehicle"},
        callSuper = false
)
public class Rental extends RecordableEntity implements IdentifiableById {

    @Id
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "rentalSeqGen"
    )
    @SequenceGenerator(
            name = "rentalSeqGen",
            sequenceName = "seq_rentals"
    )
    private BigInteger id;

    @Basic(optional = false)
    @Column(name = "pickup_date", nullable = false)
    private LocalDate pickupDate;

    @Column(name = "duration")
    private byte numberOfDays;

    @Setter
    @Column(name = "overdue_duration")
    private Byte numberOfDaysLate;

    @Setter
    @Column(name = "using_loyalty_points")
    private boolean usingLoyaltyPoints;

    @Getter
    @ManyToOne(cascade = ALL, optional = false)
    @JoinColumn(
            name = "vehicle_id",
            referencedColumnName = "id",
            nullable = false
    )
    private Vehicle vehicle;

    @Getter
    @ManyToOne(cascade = ALL, optional = false)
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            nullable = false
    )
    private Customer customer;

    public Rental(@NonNull LocalDate pickupDate, byte numberOfDays, @NonNull Vehicle vehicle,
                  @NonNull Customer customer) {

        this.pickupDate = pickupDate;
        this.numberOfDays = numberOfDays;
        this.vehicle = requireNonNull(vehicle, "Vehicle cannot be null");
        this.customer = requireNonNull(customer, "Customer cannot be null");

        validatePickupDate(pickupDate);
        validateNumberOfDays(numberOfDays);

        vehicle.addRental(this);
        customer.addRental(this);
    }

    private void validatePickupDate(@NonNull LocalDate pickupDate) {
        Validate.notNull(pickupDate, "Pickup date cannot be null");

        Validate.isTrue(pickupDate.isAfter(LocalDate.now().minusDays(1)), "Pickup date can not " +
                "be in the past");
    }

    private void validateNumberOfDays(int numberOfDays) {
        Validate.isTrue(numberOfDays > 0, "Number of days cannot be negative");
    }
}
