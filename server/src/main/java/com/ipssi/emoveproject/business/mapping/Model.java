package com.ipssi.emoveproject.business.mapping;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(
        name = "models",
        uniqueConstraints = @UniqueConstraint(columnNames = {
                "code",
                "make_id"
        })
)
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode(of = {"code", "make"}, callSuper = false)
public class Model extends RecordableEntity implements IdentifiableById {

    @Getter
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "modelSeqGen"
    )
    @SequenceGenerator(
            name = "modelSeqGen",
            sequenceName = "seq_models"
    )
    private BigInteger id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Basic(optional = false)
    @Column(nullable = false)
    private String code;

    @Getter
    @Column(name = "vehicle_type")
    private VehicleType vehicleType;

    @Getter
    @ManyToOne(cascade = ALL, optional = false)
    @JoinColumn(
            name = "make_id",
            referencedColumnName = "id",
            nullable = false
    )
    private Make make;

    @OneToMany(
            mappedBy = "model",
            cascade = ALL,
            orphanRemoval = true
    )
    private Set<Vehicle> vehicles;

    {
        vehicles = new HashSet<>(0);
    }

    public Model(
            @lombok.NonNull @NonNull String      code,
            @lombok.NonNull @NonNull VehicleType vehicleType,
            @lombok.NonNull @NonNull Make        make
    ) {
        this.code = Validate.notBlank(code, "Code cannot be null");
        this.vehicleType = vehicleType;
        this.make = make;
        make.addModel(this);
    }

    public Set<Vehicle> getVehicles() {
        return new HashSet<>(vehicles);
    }

    boolean addVehicle(@lombok.NonNull Vehicle vehicle) {
        return vehicles.add(vehicle);
    }
}
