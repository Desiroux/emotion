package com.ipssi.emoveproject.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.VehicleType;

import java.math.BigInteger;
import java.util.List;

public interface ModelDAO extends Finder<Model>, Saver<Model>, Remover<Model> {

    List<Model> findByMakeId(BigInteger makeId);

    List<Model> findByNameLike(String name);

    List<Model> findByMakeIdAndNameLike(BigInteger makeId, String name);

    List<Model> findByVehicleType(VehicleType vehicleType);
}
