package com.albema.spring.security;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.sql.Timestamp.valueOf;
import static java.util.UUID.fromString;

import lombok.Value;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

import java.util.function.Function;

@Component
public class TokenEncryptionManager {

    static
    private final char COMPONENT_DELIMITER = '#';

    public TokenEncryptionComponents toTokenEncryptionComponents(
            UUID uuid,
            String emailAddress,
            Timestamp timestamp
    ) {
        final TokenEncryptionComponents components;

        components = new TokenEncryptionComponents(uuid, emailAddress, timestamp);

        return components;
    }

    public TokenEncryptionComponents fromEncryptedString(
            String encryptedString,
            Function<String, String> decryptionFunction
    ) {
        final String[] decryptedStrings;
        TokenEncryptionComponents tokenEncryptionComponents;

        decryptedStrings = decryptionFunction.apply(encryptedString)
                .split(valueOf(COMPONENT_DELIMITER));
        try {
            tokenEncryptionComponents = new TokenEncryptionComponents(
                    fromString(decryptedStrings[0]),
                    decryptedStrings[1],
                    valueOf(decryptedStrings[2])
            );
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            tokenEncryptionComponents = null;
        }

        return tokenEncryptionComponents;
    }

    public String toEncryptedString(
            TokenEncryptionComponents tokenEncryptionComponents,
            Function<String, String> encryptionFunction
    ) {
        final String key;

        key = format(
                "%s%4$c%s%4$c%s",
                tokenEncryptionComponents.uuid,
                tokenEncryptionComponents.emailAddress,
                tokenEncryptionComponents.timestamp,
                COMPONENT_DELIMITER
        );

        return encryptionFunction.apply(key);
    }

    @Value
    public static class TokenEncryptionComponents {
        private UUID uuid;
        private String emailAddress;
        private Timestamp timestamp;
    }
}