package com.albema.spring.validation;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
public @interface Mapping {

    String attribute();

    String entityProperty() default "";
}
