package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

class ExistingEntityValidator extends BusinessDataValidator<ExistingEntity> {

    @Override
    @Transactional(readOnly = true)
    public boolean isValid(
            Serializable o,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        final String stm;

        stm = "SELECT CASE COUNT(*) WHEN 0 THEN FALSE ELSE TRUE END "
                + "FROM " + getConstraintAnnotation().value().getName() + " "
                + "WHERE " + getConstraintAnnotation().identifierProperty() + "=:value";

        return (boolean) getCurrentSession().createQuery(stm)
                .setParameter("value", o)
                .uniqueResult();
    }

    ExistingEntityValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
