package com.albema.spring.validation;

import com.albema.common.orm.entity.identifiable.IdentifiableById;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Inherited
@Constraint(validatedBy = ExistingEntityValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.PARAMETER,
})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExistingEntity {

    Class<? extends IdentifiableById> value();

    String identifierProperty() default "id";

    String message() default "ExistingEntity";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
