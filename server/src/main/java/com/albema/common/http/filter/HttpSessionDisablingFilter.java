package com.albema.common.http.filter;

import org.slf4j.Logger;

import static java.util.Collections.emptyList;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpHeaders.SET_COOKIE;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Collection;

public final class HttpSessionDisablingFilter implements Filter {

    static
    private final Logger logger = getLogger(HttpSessionDisablingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(
            ServletRequest servletRequest,
            ServletResponse servletResponse,
            FilterChain filterChain
    ) throws IOException, ServletException {
        filterChain.doFilter(
                new SessionlessRequest(
                        (HttpServletRequest) servletRequest
                ),
                new SessionlessResponse(
                        (HttpServletResponse) servletResponse
                )
        );
    }

    @Override
    public void destroy() {
    }

    static
    private class SessionlessRequest extends HttpServletRequestWrapper {

        private SessionlessRequest(HttpServletRequest request) {
            super(request);
            request.getSession().invalidate();
        }

        @Override
        public HttpSession getSession() {
            return null;
        }

        @Override
        public HttpSession getSession(boolean create) {
            return null;
        }
    }

    static
    private class SessionlessResponse extends HttpServletResponseWrapper {

        private SessionlessResponse(HttpServletResponse response) {
            super(response);
            setHeader(SET_COOKIE, null);
        }

        @Override
        public void setHeader(String name, String value) {
            if(SET_COOKIE.equals(name))
                return;
            super.setHeader(name, value);
        }

        @Override
        public void addHeader(String name, String value) {
            if(SET_COOKIE.equals(name))
                return;
            super.addHeader(name, value);
        }

        @Override
        public String getHeader(String name) {
            if(SET_COOKIE.equals(name))
                return null;
            return super.getHeader(name);
        }

        @Override
        public Collection<String> getHeaders(String name) {
            if(SET_COOKIE.equals(name))
                return emptyList();
            return super.getHeaders(name);
        }
    }
}