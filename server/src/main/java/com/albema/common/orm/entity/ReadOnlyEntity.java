package com.albema.common.orm.entity;

import org.hibernate.annotations.Immutable;

@SuppressWarnings("unused")
@Immutable
public abstract class ReadOnlyEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;
}
