package com.albema.common.util;

import java.math.BigInteger;

import static java.lang.Math.log10;
import static java.math.BigInteger.*;

@SuppressWarnings("unused")
public final class MathUtils {

    private MathUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    static
    public long digitCount(long number) {
        return number == 0 ? 1 : (long) log10(number) + 1;
    }

    static
    public long digitCount(BigInteger number) {
        long count;
        BigInteger[] resultAndRemainder;

        if (number.equals(ZERO))
            return 1;
        count = 0;
        do {
            resultAndRemainder = number.divideAndRemainder(TEN);
            number = resultAndRemainder[0];
            count++;
        } while (number.compareTo(ZERO) != 0);

        return count;
    }
}