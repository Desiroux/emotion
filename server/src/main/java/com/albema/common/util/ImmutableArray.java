package com.albema.common.util;

import java.io.Serializable;
import java.util.Iterator;

@SuppressWarnings("unused")
public class ImmutableArray<T> implements Serializable, Iterable<T> {

    private final T[] values;

    @SafeVarargs
    public ImmutableArray(T... values) {
        this.values = values;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int offset;

            @Override
            public boolean hasNext() {
                return offset < values.length;
            }

            @Override
            public T next() {
                return values[offset++];
            }
        };
    }

    public T get(int offset) throws ArrayIndexOutOfBoundsException {
        if (offset < 0 || offset >= values.length)
            throw new ArrayIndexOutOfBoundsException(offset);
        return values[offset];
    }

    public int size() {
        return values.length;
    }
}