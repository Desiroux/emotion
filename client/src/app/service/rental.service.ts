import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Rental} from "../../model/input/Rental";
import {Service} from "./service";
import {RentalCreation} from "../../model/output/RentalCreation";
import {RentalUpdate} from "../../model/output/RentalUpdate";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class RentalService extends Service {

  getAll(): Observable<Rental[]> {
    return this.httpClient.get<Rental[]>(
      'http://' + this.apiConfig.server + '/rentals'
    );
  }

  getOne(id: number): Observable<Rental> {
    return this.httpClient.get<Rental>(
      'http://' + this.apiConfig.server + '/rentals/' + id
    );
  }

  getByCustomerId(customerId: number): Observable<Rental[]> {
    return this.httpClient.get<Rental[]>(
      `http://${this.apiConfig.server}/customers/${customerId}/rentals`
    );
  }

  getOneByCustomerId(
    rentalId: number,
    customerId: number
  ): Observable<Rental> {
    return this.httpClient.get<Rental>(
      `http://${this.apiConfig.server}/customers/${customerId}/rentals/${rentalId}`
    );
  }

  create(rentalCreation: RentalCreation): Observable<Rental> {
    return this.httpClient.post<Rental>(
      'http://' + this.apiConfig.server + '/rentals/',
      rentalCreation
    );
  }

  createForCustomer(rentalCreation: RentalCreation, cutomerId: number): Observable<Rental> {
    return this.httpClient.post<Rental>(
      'http://' + this.apiConfig.server + '/customers/' + cutomerId + '/rentals',
      rentalCreation
    );
  }

  update(id: number, update: RentalUpdate): Observable<void> {
    return this.httpClient.patch<void>(
      `http://${this.apiConfig.server}/rentals/${id}`,
      update
    );
  }

  deleteOne(id: number): Observable<void> {
    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/rentals/${id}`
    );
  }

  deleteSome(ids: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    ids.forEach(
      id =>
        httpParams = httpParams.append(
          'ids', id.toString()
        )
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/rentals`,
      {
        params: httpParams
      }
    );
  }
}
