import {Injectable} from '@angular/core';
import {Service} from "./service";
import {TokenCreation} from "../../model/output/TokenCreation";
import {Token} from "../../model/input/Token";
import {Observable} from "rxjs/Observable";
import {CustomerCreation} from "../../model/output/CustomerCreation";
import {Customer} from "../../model/input/Customer";
import {AdminCreation} from "../../model/output/AdminCreation";
import {Admin} from "../../model/input/Admin";
import {CustomerUpdate} from "../../model/output/CustomerUpdate";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class UserService extends Service {

  getAllCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(
      'http://' + this.apiConfig.server + '/customers'
    );
  }

  getOneCustomer(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(
     `http://${this.apiConfig.server}/customers/${id}`
    );
  }

  createCustomer(customerCreation: CustomerCreation): Observable<Customer> {
    return this.httpClient.post<Customer>(
      'http://' + this.apiConfig.server + '/customers',
      customerCreation
    );
  }

  createAdmin(adminCreation: AdminCreation): Observable<Admin> {
    return this.httpClient.post<Admin>(
      `http://${this.apiConfig.server}/admins`,
      adminCreation
    );
  }

  update(id: number, newPassword: string): Observable<void> {
    return this.httpClient.patch<void>(
      `http://${this.apiConfig.server}/users/${id}`,
      newPassword
    );
  }

  updateCustomer(id: number, update: CustomerUpdate): Observable<void> {
    return this.httpClient.patch<void>(
      `http://${this.apiConfig.server}/customers/${id}`,
      update
    );
  }

  createToken(tokenCreation: TokenCreation): Observable<Token> {
    return this.httpClient.post<Token>(
      'http://' + this.apiConfig.server + '/tokens',
      tokenCreation
    );
  }

  deleteOneCustomer(id: number): Observable<void> {
    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/customers/${id}`
    );
  }

  deleteCustomers(ids: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    ids.forEach(
      id =>
        httpParams = httpParams.append(
          'ids', id.toString()
        )
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/customers`,
      {
        params: httpParams
      }
    );
  }

  deleteOneAdmin(id: number): Observable<void> {
    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/admins/${id}`
    );
  }

  deleteAdmins(ids: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    ids.forEach(
      id =>
        httpParams = httpParams.append(
          'ids', id.toString()
        )
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/admins`,
      {
        params: httpParams
      }
    );
  }
}
