import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Vehicle} from "../../../../../model/input/Vehicle";

@Component({
  selector: 'app-admin-vehicles',
  templateUrl: './admin-vehicles.component.html',
  styleUrls: ['./admin-vehicles.component.css']
})
export class AdminVehiclesComponent implements OnInit {

  vehicles: Vehicle[];

  selectedIds: number[];

  constructor(private readonly vehicleService: VehicleService) {
    this.vehicleService.getAll()
                       .subscribe(vehicles => this.vehicles = vehicles);

    this.selectedIds = [];
  }

  ngOnInit() {
  }

  onCheck(event: any, id: number): void {
    let index: number;
    if(event.target.checked)
      this.selectedIds.push(id);
    else if((index = this.selectedIds.indexOf(id)) != -1)
      this.selectedIds.splice(index);
  }

  deleteVehicles(): void {
    this.vehicleService.delete(this.selectedIds)
                       .subscribe(() => {
                         for(let vehicle of this.vehicles)
                           if(this.selectedIds.indexOf(vehicle.id) != -1)
                             this.vehicles.splice(this.vehicles.indexOf(vehicle));
                         this.selectedIds = [];
                       });
  }
}
