import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../../service/user.service";
import {Customer} from "../../../../../model/input/Customer";

@Component({
  selector: 'app-admin-customers',
  templateUrl: './admin-customers.component.html',
  styleUrls: ['./admin-customers.component.css']
})
export class AdminCustomersComponent implements OnInit {

  customers: Customer[];

  selectedIds: number[];

  constructor(private readonly userService: UserService) {
    this.userService
        .getAllCustomers()
        .subscribe(customers => this.customers = customers);

    this.selectedIds = [];
  }

  ngOnInit() {
  }

  onCheck(event: any, id: number): void {
    let index: number;
    if(event.target.checked)
      this.selectedIds.push(id);
    else if((index = this.selectedIds.indexOf(id)) != -1)
      this.selectedIds.splice(index);
  }

  deleteCustomers(): void {
    this.userService.deleteCustomers(this.selectedIds)
        .subscribe(() => {
          for (const customer of this.customers)
            if (this.selectedIds.indexOf(customer.id) != -1)
              this.customers.splice(this.customers.indexOf(customer));
          this.selectedIds = [];
        });
  }
}
