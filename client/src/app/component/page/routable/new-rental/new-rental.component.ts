import {OnInit, Component, Input} from '@angular/core';
import {RentalCreation} from "../../../../../model/output/RentalCreation";
import {RentalService} from "../../../../service/rental.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TokenHolder} from "../../../../helper/token-holder.service";
import {Vehicle} from "../../../../../model/input/Vehicle";
import {VehicleService} from "../../../../service/vehicle.service";
import {Customer} from "../../../../../model/input/Customer";

@Component({
  selector: 'app-new-rental',
  templateUrl: './new-rental.component.html',
  styleUrls: ['./new-rental.component.css']
})
export class NewRentalComponent implements OnInit {

  vehicle: Vehicle;

  minDate: Date;

  @Input()
  rentalCreation: RentalCreation;

  constructor(
    private readonly rentalService: RentalService,
    private readonly vehicleService: VehicleService,
    private readonly tokenHolder: TokenHolder,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {
    this.resetRentalCreation();

    this.vehicleService.getOne(this.route.snapshot.params['vehicleId'])
                       .subscribe(vehicle => this.vehicle = vehicle);
  }

  ngOnInit() {
  }

  get enoughLoyaltyPoints(): boolean {
    let customer: Customer;
    let requiredLoyaltyPoints: number;

    customer = (this.tokenHolder.token.user as Customer);
    requiredLoyaltyPoints = this.vehicle.loyaltyPoints * this.rentalCreation.duration;

    return customer.loyaltyPoints >= requiredLoyaltyPoints;
  }

  createRental(): void {
    this.rentalCreation.duration = Math.floor(this.rentalCreation.duration);
    this.rentalService
        .createForCustomer(this.rentalCreation, this.rentalCreation.customerId)
        .subscribe(rental => {
          (this.tokenHolder.token.user as Customer).rentalIds.push(rental.id);
          this.router.navigate(['location', rental.id]);
        });

  }

  private resetRentalCreation(): void {

    let tomorrow;

    tomorrow = new Date;

    tomorrow.setDate(tomorrow.getDate() + 1);

    this.minDate = new Date(tomorrow.getTime());

    this.rentalCreation = {
      vehicleId: this.route.snapshot.params['vehicleId'],
      customerId: this.tokenHolder.token.user.id,
      pickupDate: this.route.snapshot.queryParams['pickupDate'],
      duration: this.route.snapshot.queryParams['numberOfDay'] || 1,
      usingLoyaltyPoints: false,
    };
  }
}
