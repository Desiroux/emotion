import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Make} from "../../../../../model/input/Make";
import {VehicleType} from "../../../../../model/input/Model";
import {Memoize} from "typescript-memoize";

@Component({
  selector: 'app-makes',
  templateUrl: './makes.component.html',
  styleUrls: ['./makes.component.css']
})
export class MakesComponent implements OnInit {

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  @Memoize()
  get makes(): Observable<Make[]> {
    return this.vehicleService.getMakesByVehicleType(
      VehicleType[
        VehicleType[
          this.route.snapshot.params['type']
        ]
      ]
    );
  }
}
