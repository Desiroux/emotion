import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Make} from "../../../../../model/input/Make";

@Component({
  selector: 'app-admin-makes',
  templateUrl: './admin-makes.component.html',
  styleUrls: ['./admin-makes.component.css']
})
export class AdminMakesComponent implements OnInit {

  makes: Make[];

  selectedIds: number[];

  constructor(private readonly vehicleService: VehicleService) {
    this.vehicleService.getAllMakes()
                       .subscribe(makes => this.makes = makes);

    this.selectedIds = [];
  }

  ngOnInit() {
  }

  onCheck(event: any, id: number): void {
    let index: number;
    if(event.target.checked)
      this.selectedIds.push(id);
    else if((index = this.selectedIds.indexOf(id)) != -1)
      this.selectedIds.splice(index);
  }

  deleteMakes(): void {
    this.vehicleService.deleteMakes(this.selectedIds)
                       .subscribe(
                         () => {
                           for(let make of this.makes)
                             if(this.selectedIds.indexOf(make.id) != -1)
                              this.makes.splice(this.makes.indexOf(make));
                           this.selectedIds = [];
                         });
  }
}
