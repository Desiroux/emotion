import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Make} from "../../../../../model/input/Make";

@Component({
  selector: 'app-make-autocomplete',
  templateUrl: './make-autocomplete.component.html',
  styleUrls: ['./make-autocomplete.component.css']
})
export class MakeAutocompleteComponent implements OnInit {

  makes: Make[];

  @Input()
  make: Make;

  @Output("makeChanged")
  eventEmitter : EventEmitter<Make>;

  constructor(private readonly vehicleService: VehicleService) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit(): void {
    this.reload();
  }

  reload(): void {
    this.vehicleService
        .getAllMakes()
        .subscribe(makes => this.makes = makes);
  }

  emit(): void {
    this.eventEmitter.emit(this.make);
  }
}
