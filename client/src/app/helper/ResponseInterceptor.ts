import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

const HEADER_CONTENT_TYPE: string = 'Content-Type';

const CONTENT_TYPE_APPLICATION_JSON: string = 'application/json';

const REGEX_DATE: RegExp = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;

export class ResponseInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).map(event => {
      if(event instanceof HttpResponse &&
        (event.headers.get(HEADER_CONTENT_TYPE) || '')
                      .includes(CONTENT_TYPE_APPLICATION_JSON)) {

        let body: Object;
        let update: boolean;

        body = event.body as Object;

        if(body != null)
          for(let key in body)
            if(typeof body[key] === 'string' &&
              body[key].match(REGEX_DATE)) {
              body[key] = new Date(body[key]);
              update = true;
            }

        if(update)
          event = event.clone({
            body: body,
          });
      }

      return event;
    });
  }
}
