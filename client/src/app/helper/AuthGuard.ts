import {ActivatedRouteSnapshot, CanActivate, NavigationEnd, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Type} from "../../model/input/User";
import {TokenHolder} from "./token-holder.service";
import {SecurityUtils} from "../../utils/SecurityUtils";
import {Location} from "@angular/common";

@Injectable()
export abstract class AuthGuard implements CanActivate {

  constructor(
    protected readonly tokenHolder: TokenHolder,
    protected readonly router: Router,
    protected readonly location: Location
  ) {}

  protected abstract support(): Type;

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if(SecurityUtils.isGranted(this.tokenHolder.token, this.support()))
      return true;

    const currentUrl: string = state.url;

    if(this.tokenHolder.token != null)
      this.router.navigateByUrl(
        '/403',
        {
          skipLocationChange: true,
          replaceUrl: false,
        }
      ).then(() => this.location.replaceState(currentUrl));
    else
      this.router.navigateByUrl(
        '/401',
        {
          skipLocationChange: true,
          replaceUrl: false,
        }
    ).then(() => this.location.replaceState(currentUrl));

    return false;
  }
}
