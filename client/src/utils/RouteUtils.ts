import {Router} from "@angular/router";

export class RouteUtils {

  private constructor() {}

  static refresh(router: Router): void {

    const currentPath: string = router.url;

    router.navigateByUrl(
      '/',
      {
        replaceUrl: false,
        skipLocationChange: true,
      }
    ).then(
      () =>
        router.navigateByUrl(
          currentPath,
          {
            replaceUrl: false,
            skipLocationChange: true,
          }
        )
    );
  }
}
