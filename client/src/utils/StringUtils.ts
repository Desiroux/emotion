export class StringUtils {

  private constructor() {}

  static isBlank(str: string): boolean {
    return str == null || str.length == 0 || str.trim().length == 0;
  }

  static areBlank(strs: string[]): boolean {
    for(let str of strs)
      if(StringUtils.isBlank(str))
        return true;
    return false;
  }
}
