
export enum Type {
  GDPR = "GDPRUpdate",
  DETAILS = "CustomerDetailsUpdate",
}

export interface CustomerUpdate {
  __type__: Type
}
