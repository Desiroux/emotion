import {Type, VehicleUpdate} from "./VehicleUpdate";

export interface RentalPriceAndLoyaltyPointsUpdate extends VehicleUpdate {
  rentalPrice: number
  loyaltyPoints: number
}

export function create(
  rentalPrice: number,
  loyaltyPoints: number
): RentalPriceAndLoyaltyPointsUpdate {
  return {
    __type__: Type.RENTALPRICE_AND_LOYALTYPOINTS,
    rentalPrice: rentalPrice,
    loyaltyPoints: loyaltyPoints,
  };
}
