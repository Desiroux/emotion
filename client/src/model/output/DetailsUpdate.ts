import {CustomerUpdate, Type} from "./CustomerUpdate";

export interface DetailsUpdate extends CustomerUpdate {
  address: string
  zipCode: string
  city: string
}

export function create(
  address: string,
  zipCode: string,
  city: string
): DetailsUpdate {
  return {
    __type__: Type.DETAILS,
    address: address,
    zipCode: zipCode,
    city: city,
  }
}
