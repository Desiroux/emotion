import {Type, VehicleUpdate} from "./VehicleUpdate";

export interface KilometerageUpdate extends VehicleUpdate {
  kilometerage: number
}

export function create(kilometerage: number): KilometerageUpdate {
  return {
    __type__: Type.KILOMETERAGE,
    kilometerage: kilometerage,
  };
}
