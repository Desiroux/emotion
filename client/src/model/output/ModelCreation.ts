import {VehicleType} from "../input/Model";

export interface ModelCreation {
  makeId: number
  vehicleType: VehicleType,
  code: string
  name: string
}
