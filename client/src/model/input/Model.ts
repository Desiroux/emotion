import {IdentifiableById} from "./IdentifiableById";
import {Make} from "./Make";

export enum VehicleType {
  CAR,
  SCOOTER,
}

export interface Model extends IdentifiableById {
  code: string
  name: string
  vehicleType: VehicleType
  make: Make
  vehicleIds: number[]
}
