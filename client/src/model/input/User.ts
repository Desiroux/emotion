import {IdentifiableById} from "./IdentifiableById";

export enum Type {
  CUSTOMER = "CUSTOMER",
  ADMIN = "ADMIN",
}

export interface User extends IdentifiableById {
  __type__: Type
  emailAddress: string
}
